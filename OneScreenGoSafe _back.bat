@echo off
echo
FOR /F "tokens=5" %%T IN ('netstat -a -n -o ^| findstr 9003 ') DO (
SET /A ProcessId=%%T) &GOTO SkipLine                                                   
:SkipLine                                                                             
echo ProcessId to kill = %ProcessId%
taskkill /f /pid %ProcessId%
cd  %appdata%
set "myDir=OneScreen PreScreen"
cd %myDir%
cd /d GoSafePreScreen 
Start OneScreenGoSafe_serv.exe
