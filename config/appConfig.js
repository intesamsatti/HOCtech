let settings = require("../settings");

let appConfig = {
    baseURL: settings.app_host + ":" + settings.app_port,
    hostAddress: settings.app_host,
    hostIPAddress: settings.app_ip,
    port: settings.app_port,
    certFileName: settings.ssl_cert,
    keyFileName: settings.ssl_key,
    bundleFileName: settings.ssl_bundle,
    jwtKey : settings.app_jwt_key,
    uploadDirPath: settings.path_uploads,
    serverPort: `:${settings.app_port}/`,
    qrCodePath: settings.path_qrcodes,
    qrCodeUrlPath: settings.path_qrcodes_url,
    maxWorkerProcesses: settings.max_worker_processes,
    isClusterMode: settings.is_cluster_mode,
    dbHost: settings.db_host, 
    dbUserName: settings.db_username, 
    dbPassword: settings.db_password, 
    dbName: settings.db_name, 
    dbPort: settings.db_port,
    devMode: settings.dev_mode,
    canSuperAdminLogin: settings.can_super_admin_login,
    httpRedirect: settings.http_redirect,
    license_host: settings.license_host,
    license_ip: settings.license_ip,
    license_port: settings.license_port,
    dbHost: settings.db_host,
    db_port: settings.db_port,
    db_name: settings.db_name,
    db_username: settings.db_username,
    db_password: settings.db_password
}

module.exports = appConfig;