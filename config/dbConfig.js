var mysql = require('mysql');
//var os = require('os');
var mysqlPool = (function(){
    var pool = null;

    var getMysqlPool = function(){
        if(pool == null){
            pool = mysql.createPool({
                connectionLimit : 1000,
                connectTimeout: 30000,
                acquireTimeout: 30000,
                host     : require("./secret").getDBHost(),
                user     : require("./secret").getDBUserName(),
                password : require("./secret").getDBPassword(),
                database : require("./secret").getDBName(),
                port     : require("./secret").getDBPort(), 
                debug    : false,
                charset  : 'utf8mb4',
                insecureAuth : true
            });
        }
        return pool;
    };

    return {
        getMysqlPool: getMysqlPool
    }
})();

module.exports = mysqlPool;