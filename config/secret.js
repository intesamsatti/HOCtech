
const appConfig = require('./appConfig');

var secret = {

    getDBHost: () => {
        return appConfig.dbHost;
    },
    
    getDBUserName: () => {
        return appConfig.dbUserName;
    },
    
    getDBPassword: () => {  
        return appConfig.dbPassword;
    },
    
    getDBName: () => {
        return appConfig.dbName;
    },

    getDBPort: () => {
        return appConfig.dbPort;
    },

    getJwtKey: () => {
        return appConfig.jwtKey;
    }
}

module.exports = secret;