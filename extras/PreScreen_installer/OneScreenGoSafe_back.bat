@echo off
echo
set NODE_ENV=production
FOR /F "tokens=5" %%T IN ('netstat -a -n -o ^| findstr 9003 ') DO (
SET /A ProcessId=%%T) &GOTO SkipLine                                                   
:SkipLine                                                                             
echo ProcessId to kill = %ProcessId%
taskkill /f /pid %ProcessId%
FOR /F "tokens=5" %%T IN ('netstat -a -n -o ^| findstr 5000 ') DO (
SET /A ProcessId=%%T) &GOTO SkipLine                                                   
:SkipLine                                                                             
echo ProcessId to kill = %ProcessId%
taskkill /f /pid %ProcessId%

cd GoSafePreScreen 
Start OneScreenGoSafe_serv.exe
