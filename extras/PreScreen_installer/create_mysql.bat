%1 %2
ver|find "5.">nul&&goto :st
mshta vbscript:createobject("shell.application").shellexecute("%~s0","goto :st","","",1)(window.close)&goto :eof
 
:st
copy "%~0" "%windir%\system32\"

if "%3" == "h" goto :BEGIN  
mshta vbscript:createobject("wscript.shell").run("""%~dp0%~nx0"" goto :st h",0)(window.close)&&exit  
  
:BEGIN

@echo off
title Start

setlocal enabledelayedexpansion
%~d0
cd %~dp0
set "BASE_DIR=%cd%"
rem set server port into environment variables
set "SET_PORT_BAT=%cd%\setport.bat"
if not exist "%SET_PORT_BAT%" echo Can Not Find Server Port Config File... & goto exit
call "%SET_PORT_BAT%"
cd mysql

set "MySQL8_PRESCREEN_HOME=%cd%"
set "MY_INI=%MySQL8_PRESCREEN_HOME%\my.ini"
set "MY_INI_BAK=%MySQL8_PRESCREEN_HOME%\my_bak.ini"
if exist "%MY_INI%" (del /q/f "%MY_INI%")

if not exist "C:\Windows\System32\msvcr120.dll" (
xcopy "%MySQL8_PRESCREEN_HOME%\lib\msvcr120.dll" "C:\Windows\System32\" /s /y
)

set "STR1=$MySQL8_PRESCREEN_HOME"
set "STR2=%MySQL8_PRESCREEN_HOME:\=/%"
echo here I am 3
(for /f "tokens=* delims= usebackq" %%i in ("%MY_INI_BAK%") do (
	set s=%%i
	set s=!s:%STR1%=%STR2%!
	echo !s!
))>>"%MY_INI%"

cd bin
for /f "skip=3 tokens=4" %%i in ('sc query MySQL8_PRESCREEN') do set "zt=%%i" &goto exist
:exist
if /i "%zt%"=="RUNNING" (  
    net stop MySQL8_PRESCREEN
    sc delete MySQL8_PRESCREEN
) else if /i "%zt%"=="STOPPED" (
    mysqld remove MySQL8_PRESCREEN
    sc delete MySQL8_PRESCREEN
)
mysqld --install MySQL8_PRESCREEN --defaults-file="%MY_INI%"

setlocal enabledelayedexpansion
for /f "eol=* tokens=*" %%i in ('netstat -an -o ^| findstr "%MYSQL_PORT%"') do (
set a=%%i
set a=!a:~69,10!
if !a!  NEQ 0 (taskkill /f /pid !a!)
)


echo MYSQL LISTING ON %MYSQL_PORT% ....
FOR /F "tokens=5 delims= " %%P IN ('netstat -a -n -o ^| findstr :%MYSQL_PORT%.*LISTENING') DO @echo %MYSQL_PORT% &goto :exit

for /f "skip=3 tokens=4" %%i in ('sc query mysql') do set "zt=%%i" &goto :next   
:next  
if /i "%zt%"=="RUNNING" (  
    echo MYSQL RUNNING RESTARTING ...... 
    net stop mysql
)

net start MySQL8_PRESCREEN

%~d0
cd %~dp0
set "b=%cd%"
cd mysql\bin
setlocal disabledelayedexpansion
mysql -uroot -p123pre!@# -h127.0.0.1 -P3308 prescreen<"%b%\\GoSafePreScreen\\schema\\database.sql" 
mysql -uroot -p123pre!@# -h127.0.0.1 -P3308 prescreen<"%b%\\GoSafePreScreen\\schema\\stored_procedures.sql"
mysql -uroot -p123pre!@# -h127.0.0.1 -P3308 prescreen<"%b%\\GoSafePreScreen\\schema\\schema.sql"
mysql -uroot -p123pre!@# -h127.0.0.1 -P3308 prescreen<"%b%\GoSafePreScreen\\scripts\add_super_admin_values.sql
netsh advfirewall firewall add rule name=prescreen dir=in action=allow protocol=TCP localport=9003


:exit
echo Update complete press any key to exit...
