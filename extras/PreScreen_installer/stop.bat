@echo off
echo

set serverPid=
for /F "tokens=5 delims= " %%P in ('netstat -a -n -o ^| findstr /E :9003 ') do set serverPid=%%P
if not "%serverPid%" == "" (
  taskkill /PID %serverPid%
) else (
   echo Server is not running.
)

for /F "tokens=5 delims= " %%P in ('netstat -a -n -o ^| findstr /E :5000 ') do set serverPid=%%P
if not "%serverPid%" == "" (
  taskkill /PID %serverPid%
) else (
   echo Server is not running.
)

FOR /F "tokens=5" %%T IN ('netstat -a -n -o ^| findstr 9003 ') DO (
SET /A ProcessId=%%T) &GOTO SkipLine                                                   
:SkipLine                                                                             
echo ProcessId to kill = %ProcessId%
taskkill /f /pid %ProcessId%

@echo off
echo
FOR /F "tokens=5" %%T IN ('netstat -a -n -o ^| findstr 5000') DO (
SET /A ProcessId=%%T) &GOTO SkipLine                                                   
:SkipLine                                                                             
echo ProcessId to kill = %ProcessId%
taskkill /f /pid %ProcessId%

for /f "skip=3 tokens=4" %%i in ('sc query MySQL8_PRESCREEN') do set "zt=%%i" &goto exist
:exist
if /i "%zt%"=="RUNNING" (  
    net stop MySQL8_PRESCREEN
)



