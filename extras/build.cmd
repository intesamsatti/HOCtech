echo off 
cd ..
cd ..

echo "building preScreen frontend"
xcopy GoSafePreScreenBackend\\\extras\\PreScreen_installer  PreScreen_installer  /r /y  /i /e 
Rmdir /Q /S GoSafePreScreenBackend\\public
mkdir GoSafePreScreenBackend\\public
cd GoSafePreScreenFrontend
echo "running npm "
call npm i
call  npm run build
xcopy build ..\\GoSafePreScreenBackend\\public /r /y  /i /e
cd ..
cd GoSafePreScreenBackend
echo "<---backend git head --->"

echo "running npm "
call npm i
cd ..
Rmdir /Q /S %cd%\\PreScreen_installer\\GoSafePreScreen
mkdir PreScreen_installer\\GoSafePreScreen
xcopy GoSafePreScreenBackend PreScreen_installer\\GoSafePreScreen /r /y /i /e
cd /d PreScreen_installer
echo %cd%
copy  "%cd%\\OneScreenGoSafe_serv.exe" "%cd%\\GoSafePreScreen\\" /y 
rename "%cd%\\GoSafePreScreen\\settings.json" "settings_temp.json" 
Rmdir /Q /S %cd%\\Installer
mkdir  %cd%\\dist
Rmdir /Q /S %cd%\\GoSafePreScreen\\extras
.\\NSIS\\Bin\\makensis.exe /V3 /P4  "OneScreenPreScreen.nsi"
%cd%\\signtool.exe sign /a /f %cd%\\ClaryOneTouch.p12 /p GWyEnxXug /tr http://timestamp.digicert.com  "%cd%\\dist\\OneScreenGoSafePrescreen_*.exe"
cd ..

echo "done building installer"


 


