//var createError = require('http-errors');
var express = require('express');
const compression = require('compression');
var app = express();
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var logger = require('morgan');
var cors = require('cors');
var indexRouter = require('./routes/router');
var routesValidate = require('./middleware/routesValidate');
var app = express();
//app.use(compression());
app.use(compression({threshold: 0}));
app.set('etag', 'weak');
var fs = require('fs-extra');
const https = require('https');
const cluster = require('cluster');
const http = require('http');
const appConfig = require('./config/appConfig');
const PORT = appConfig.serverPort.substring(appConfig.serverPort.lastIndexOf(':') + 1, appConfig.serverPort.lastIndexOf('/'));
var uploadDir = './public/uploads/';
//let multer = require('multer');
//const scheduler = require('./routes/scheduler/scheduler');
//const request = require("request");
//const https = require('https');
//var FormData = require('form-data');
const corsOptions = {
  exposedHeaders: '*',
};
app.use(cors(corsOptions));
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '100mb',
  parameterLimit: 2000000
}));
app.use(express.static(path.join(__dirname, 'public'), {
  etag: true,
  setHeaders: function(res, path) {
    if (path.endsWith('.png') || path.endsWith('.jpg') || path.endsWith('.jpeg') || path.endsWith('.svg')) {
      res.setHeader('Cache-Control', 'private, max-age=10800'); //3h
    } else {
      res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate');
      res.setHeader('Pragma', 'no-cache');
      res.setHeader('Surrogate-Control', 'no-cache');
      res.setHeader('Expires', '0');
    }
  },
}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.all('/api/v1/*', routesValidate.validate);
app.use('/', indexRouter);

//main route handling for production
app.get("/main", (req, res) => {
  res.sendFile(path.resolve(__dirname, "public", "index.html"));
});

app.get("*", (req, res)=>{
  res.sendFile(path.resolve(__dirname, "public", "index.html"), ()=>{
  });
});

// var storage = multer.diskStorage({
//   destination: function(req,file,cb) {
//     fs.mkdirSync(uploadDir, { recursive: true })
//     cb(null, uploadDir);
//   },
//   filename: function(req, file, cb){
//     var fileName = file.originalname;
//     cb(null,fileName);
//   }
// });

// var uploads = multer({
//   storage: storage,
//   limits: {
//     fileSize: 1024 * 2048
//   }
// });

//var cplUpload = uploads.single("image");
//app.post('/api/v1/upload/CompanyProfile', function(req,res){
//    cplUpload(req, res, function(err,fileName){
//        if(err){
//          res.send(err);
//          return;
//        }
//        res.json({
//          "statusCode" : 0,
//          "data" :{
//              "message" :  'success',
//          }
//       });
//    });
//});

// var faceStorage = multer.memoryStorage();
// var faceDetection = multer({
//   storage: storage,
//   fileFilter: (req, file, cb) => {
//     if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
//       cb(null, true);
//     } else {
//       cb(null, false);
//       return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
//     }
//   }
// });

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
app.use(function(err, req, res, next) {
  // set locals, only provding error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.header("Access-Control-Allow-Headers", "Etag");
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/*
 * multi-origin setting ***/
app.use((req, res, next) => {
  "use strict";
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
  //res.header('Access-Control-Allow-Credentials', true);
  res.header("Access-Control-Allow-Headers", "*");
  //res.set('Connection', 'close');
  if(req.method == 'OPTIONS') {
    res.send(200);
  } else {
    next();
  }
});
/******** end multi-origin setting ***/

const privateKey = fs.readFileSync(appConfig.keyFileName).toString(); //, 'utf8'
const certificate = fs.readFileSync(appConfig.certFileName).toString(); //, 'utf8'
let credentials = {key: privateKey, cert: certificate};
if(appConfig.bundleFileName){
  let caCertificates = fs.readFileSync(`${appConfig.bundleFileName}`).toString(); //, 'utf8'
  credentials.ca = caCertificates;
}
/* 
 * Multi-core handling. TODO:: pass env variable to start server in cluster mode or fork mode.
 */ 
let numCPUs = require('os').cpus().length - 1;
if(appConfig.maxWorkerProcesses && appConfig.maxWorkerProcesses > 0){
 numCPUs = appConfig.maxWorkerProcesses - 1;
}
if(numCPUs == 0){
 numCPUs = 1;
}

let isClusterMode = appConfig.isClusterMode;
if(isClusterMode){
  if (process.env.NODE_APP_INSTANCE === undefined || process.env.NODE_APP_INSTANCE === null || process.env.NODE_APP_INSTANCE == 0) {
    printInColor(`Master:: ${process.pid} is running`);
    
    cluster.on('exit', (worker, code, signal) => {
      printInColor(`worker:: ${worker.process.pid} died`);
    });
  } else {
    initializeServer();
    startServer();
    printInColor(`Worker:: ${process.pid} started`);
  }
} else {
  if(cluster.isMaster){
    printInColor(`Master:: ${process.pid} is running`);
    // Fork workers.
    for (let index = 0; index < numCPUs; index++) {
      cluster.fork();
    }
    
    cluster.on('exit', (worker, code, signal) => {
      printInColor(`worker:: ${worker.process.pid} died`);
    });
  } else {
    initializeServer();
    startServer();
    printInColor(`Worker:: ${process.pid} started`);
  }
}

function initializeServer(){
  /* http redirection */ 
  if(appConfig.httpRedirect){
    let hostAddress = appConfig.hostAddress.split('https://')[1] || appConfig.hostAddress;
    http.createServer(function (req, res){
      res.writeHead(308, {"Location": "https://" + req.headers['host'] + req.url});
      res.end();
    }).listen(80, `${hostAddress}`);
  }
}

function startServer(){
  const server = https.createServer(credentials, app);
  let hostAddress =  "127.0.0.1";
        let port = appConfig.port;
        if(!hostAddress) {
            console.log("error initializing server");
            return;
        }
        if(!port) {
            port = "80";
        }
        server.listen(port, hostAddress, () => {
            console.log(`Server listening at: ${hostAddress}:${port}`);
        });
  //httpsServer.listen(PORT, appConfig.hostAddress.split("https://")[1], function () {
  //  printInColor(`Server listening at: ${appConfig.hostAddress.split("https://")[1]}:${PORT}`);
  //});
}

process
    .on('unhandledRejection', (reason, p) => {
      console.error(reason, 'Unhandled Rejection at Promise', p);
    })
    .on('uncaughtException', err => {
      console.error(err, 'Uncaught Exception thrown');
    });

function printInColor(msg){
  console.log("\x1b[42m%s\x1b[0m", msg);
}
// app.listen(PORT, () => {
//   console.log(`Server listening at: localhost:${PORT}`)
// });

module.exports = app;