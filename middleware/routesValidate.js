
const utility = require('../routes/common/utility');
const {responseCodes, responseMessages} = require('../routes/common/enums');
const secret = require("../config/secret");
let jwt = require('jsonwebtoken');

var routesValidate = {
    validate: function(req, res, next){
        let token = req.headers.token;
        let {currentlanguage = "lang_en"} = req.headers;
        req.headers.currentlanguage = currentlanguage;
        //console.log(`tokken: ${token}`);
        routesValidate.isSecure(req.path).then((isSecure) => {
            if(!isSecure) {
                next();
                return;
            }
            
            if(!token) {
                utility.sendResponse(res, responseCodes.INVALID_TOKEN,responseMessages.INVALID_TOKEN);
                return;
            }
            // verify a token symmetric
            let jwtKey = secret.getJwtKey();
            jwt.verify(token, jwtKey, function(err, tokenData) {
                if(err || !tokenData){
                    utility.sendResponse(res, responseCodes.INVALID_TOKEN, responseMessages.INVALID_TOKEN);
                    return;
                }
                if(tokenData.expiry <= Date.now()){
                    utility.sendResponse(res, responseCodes.TOKEN_EXPIRED, responseMessages.INVALID_TOKEN);
                    return;
                }
                req.adminUser = tokenData;
                next();
            });
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.INVALID_TOKEN, responseMessages.INVALID_TOKEN);
            return;
        });
    },

    isSecure: (url) => {
        return new Promise ((resolve, reject) => {
            if(!url) {
                reject(false);
                return;
            }
            if((url.indexOf("license/login") == -1) &&  
               (url.indexOf("sectors") == -1) &&  
               (url.indexOf("login") == -1)  &&  
               (url.indexOf("project") == -1) &&  
               (url.indexOf("phase") == -1) &&
               (url.indexOf("/api/v1/projects") == -1) &&
               (url.indexOf("/api/v1/sectors") == -1) &&
               (url.indexOf("/api/v1/phases") == -1) &&
               (url.indexOf("/api/v1/reports") == -1)
            ) {
               resolve(true);
               return;
            }
            resolve(false);
            return;
        });
    }
}

module.exports = routesValidate;