const utility = require("./common/utility");
const { responseCodes, responseMessages, licenseStatuses } = require("./common/enums");
const dbContext = require("./common/dbContext");
const crypto = require("crypto");
const enums = require("./common/enums");
const requestIp = require("request-ip");
//const get_ip = require("ipware")().get_ip;

let Phases = {
    addPhase: (req, res) => {
        let { name, budget, quantity, projectId } = req.body;
        if (!name || !budget || !quantity || !projectId) {
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
        }
        let addPhase = `INSERT INTO phases (name, projectId, physicalProgress, cost, quantity, createdAt, updatedAt)
         VALUES ('${name}', ${projectId}, ${enums.phaseStatuses.PENDING}, ${budget}, '${quantity}', 
         '${utility.getTimeStamp()}', '${utility.getTimeStamp()}')`;
        dbContext.query(addPhase).then((response) => {
            utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)

        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    },

    getPhases: (req, res) => {
        let { projectId } = req.query
        if(!projectId){
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
            return
        }
        let getPhases = `SELECT * FROM phases WHERE projectId = ${projectId}`;
        dbContext.query(getPhases).then((response) => {
                utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    },
}

module.exports = Phases;