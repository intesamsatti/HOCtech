const utility = require("./common/utility");
const {responseCodes, responseMessages, licenseStatuses} = require("./common/enums");
const dbContext = require("./common/dbContext");
const {phaseStatuses} = require("./common/enums");
const crypto =  require("crypto");
const enums = require("./common/enums");

var mysql   = require("mysql");
const requestIp = require("request-ip");
const { getTimeStamp } = require("./common/utility");
//const get_ip = require("ipware")().get_ip;

let Projects = {

    addProjects : (req, res) => {

        //var em = req.body.email || req.query.email;
        var post  = {
            name:req.body.name,
            approvingAuthority:req.body.approvingAuthority,
            adminApprovalDate:req.body.adminApprovalDate,
            adpCost:req.body.adpCost,
            adpSno:req.body.adpSno,
            status:enums.phaseStatuses.PENDING,
            sectorId: req.body.sectorId
        }   
        if(post.name ==null){
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
        }
        var query = "insert into projects (name,sectorId,approvingAuthority,adminapprovaldate,adpcost,adpsno,status,createdAt,updatedAt)values(?,?,?,?,?,?,?,?,?);";
        var table = [ post.name,post.sectorId,post.approvingAuthority,post.adminApprovalDate,post.adpCost,post.adpSno,post.status,getTimeStamp(),getTimeStamp()];
    
        query = mysql.format(query,table);    
        dbContext.query(query).then((response) => {
            if(!response.length){
                response = []
            }
            utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
        
    
    
    },
     editProject : (req, res) => {

        //var em = req.body.email || req.query.email;
        var post  = {            
            name:req.body.name,
            approvingAuthority:req.body.approvingAuthority,
            adminApprovalDate:req.body.adminApprovalDate,
            adpCost:req.body.adpCost,
            adpSno:req.body.adpSno,
            status:phaseStatuses[req.body.status]
        }
        if(req.body == null){
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
        }
        if(req.body.id == null)
        {
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
        }
        var query = "update projects set ? , updatedAt = "+getTimeStamp()+" where id = "+req.body.id+"";
        
        //var table = [ post.name,post.budget,getTimeStamp(),getTimeStamp()];
        query = mysql.format(query,post);
    
        dbContext.query(query).then((response) => {
            if(!response.length){
                response = []
            }
            utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    }
    ,
    editProjectStatus : (req, res) => {

        //var em = req.body.email || req.query.email;
        var post  = {
            status:phaseStatuses[req.body.status]
        }
        if(req.body == null){
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
        }
        if(req.body.id == null)
        {
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
        }
        var query = "update projects set ? , updatedAt = "+getTimeStamp()+" where id = "+req.body.id+"";
        
        //var table = [ post.name,post.budget,getTimeStamp(),getTimeStamp()];
        query = mysql.format(query,post);    
        dbContext.query(query).then((response) => {
            if(!response.length){
                response = []
            }
            utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    }
    ,

    getProjects: (req, res) => {
        let {sectorId} = req.query;
        let query = `SELECT * FROM projects WHERE sectorId = ${sectorId}`;
        dbContext.query(query).then((response) => {
            if(!response.length){
                response = []
            }
            utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    },
}

module.exports = Projects;