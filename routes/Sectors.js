const utility = require("./common/utility");
const {responseCodes, responseMessages, licenseStatuses} = require("./common/enums");
const dbContext = require("./common/dbContext");
const crypto =  require("crypto");
const enums = require("./common/enums");
const requestIp = require("request-ip");
//const get_ip = require("ipware")().get_ip;

let Sectors = {

    addSector: (req, res) => {
        let { sectorName } = req.body
        if(!sectorName){
            utility.sendResponse(res, responseCodes.INVALID_PARAMS, responseMessages.INVALID_PARAMS)
            return
        }
        let getPhases = `INSERT INTO sectors (name, createdAt, updatedAt) VALUES ('${sectorName}', '${utility.getTimeStamp()}', '${utility.getTimeStamp()}')`;
        dbContext.query(getPhases).then((response) => {
                utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    },

    getSectors: (req, res) => {
        let getSectors = `SELECT * FROM sectors`;
        dbContext.query(getSectors).then((response) => {
            if(!response.length){
                response = []
            }
            utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    },
}

module.exports = Sectors;