'use strict';

const pool = require('../../config/dbConfig').getMysqlPool();
const enums = require('./enums');

class dbContext {
    constructor(){

    }

    static getDbConnection(){
        let promise = new Promise( (resolve, reject) => {
            pool.getConnection(function(err, connection){
                if(err){
                    reject(enums.responseCodes.CONNECTION_POOL_ERROR);
                    return;
                }
                resolve(connection);
                return;
            });
        });
		return promise;
    }

    static closeDbConnection(connection){
        if(connection && connection.state != 'disconnected'){
            connection.release();
        }
    }

    static query(query){
        return new Promise((resolve, reject)=> {
            pool.getConnection(function(err, connection) {
                if (err) {
                    return reject({code: enums.responseCodes.CONNECTION_POOL_ERROR, message: enums.responseMessages.CONNECTION_POOL_ERROR});
                } 
                connection.query(query, (err, result) => {
                    connection.release();
                    err? reject({code: enums.responseCodes.SQL_QUERY_ERROR, message: enums.responseMessages.SQL_QUERY_ERROR}): resolve(result);
                });
            });
        });
    }

    static queryWithValue(query, value){
        return new Promise((resolve, reject)=> {
            pool.getConnection(function(err, connection) {
                if (err) {
                    return reject({code: enums.responseCodes.CONNECTION_POOL_ERROR, message: enums.responseMessages.CONNECTION_POOL_ERROR});
                } 
                connection.query(query, value, (err, result) => {
                    connection.release();
                    err? reject({code: enums.responseCodes.SQL_QUERY_ERROR, message: enums.responseMessages.SQL_QUERY_ERROR}): resolve(result);
                });
            });
        });
    }

    static queryWithConnection(connection, query, shouldReleaseConnection = false){
        return new Promise((resolve, reject) => {
            if(!connection || connection.state == 'disconnected') return reject();
            connection.query(query, (err, result) => {
                if(shouldReleaseConnection){
                    connection.release();
                }
                err? reject({code: enums.responseCodes.SQL_QUERY_ERROR, message: enums.responseMessages.SQL_QUERY_ERROR}): resolve(result);
            });
        });
    }

    /**
     * static method to start transaction in db
     */
    static beginTransaction() {
        return new Promise((resolve, reject) => {
            pool.getConnection(function (err, connection) {
                if (err) {
                    return reject({code: enums.responseCodes.CONNECTION_POOL_ERROR, message: enums.responseMessages.CONNECTION_POOL_ERROR});
                } 
                connection.beginTransaction(function (err) {
                    if (err) {
                        connection.release();
                        reject(err);
                    } else {
                        resolve(connection);
                    }
                });
            });
        });
    }

    /**
     * static method to query during transaction 
     * @param {*} connection 
     * @param {*} query 
     */
    static queryInTransaction(connection, query) {
        return new Promise((resolve, reject) => {
            if(!connection || !query) {
                return reject(enums.responseCodes.INVALID_PARAMS);
            }

            connection.query(query, function (err, rows, fields) {
                err? reject({code: enums.responseCodes.SQL_QUERY_ERROR, message: enums.responseMessages.SQL_QUERY_ERROR}): resolve(rows);
            });
        });
    }

    /**
     * static method to query with values during transaction
     * @param {*} connection 
     * @param {*} query 
     * @param {*} value 
     */
    static queryWithValueInTransaction(connection, query, value) {
        return new Promise((resolve, reject) => {
            if(!connection || !query || !value) {
                return reject(enums.responseCodes.INVALID_PARAMS);
            }

            connection.query(query, value, function (err, rows, fields) {
                err? reject({code: enums.responseCodes.SQL_QUERY_ERROR, message: enums.responseMessages.SQL_QUERY_ERROR}): resolve(rows);
            });
        });
    }

    /**
     * static method to rollback the db transactions
     * @param {*} connection 
     */
    static rollbackTransaction(connection) {
        return new Promise((resolve, reject) => {
            if(!connection) {
                return reject(enums.responseCodes.INVALID_PARAMS);
            }

            connection.rollback(function(){
                connection.release();
                resolve();
            });
        });
    }

    /**
     * static method to  commit transaction of on db
     * @param {*} connection 
     */
    static commitTransaction(connection) {
        return new Promise((resolve, reject) => {
            if(!connection) {
                return reject(enums.responseCodes.INVALID_PARAMS);
            }
            
            connection.commit(function(err){
                connection.release();
                err? reject({code: enums.responseCodes.SQL_QUERY_ERROR, message: enums.responseMessages.SQL_QUERY_ERROR}): resolve();
            });
        });
    }
}

module.exports = dbContext;