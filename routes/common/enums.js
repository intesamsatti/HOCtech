let enums = {
    licenseStatuses: {
        "ACTIVE": 1,
        "EXPIRED": 2,
        "BLOCKED": 3,
        "DELETED": 4
    },
    phaseStatuses:{
        "PENDING": 1,
        "INPROGRESS": 2,
        "PAID": 3,
        "COMPLETED": 4
    },

    responseCodes: {
        "SUCCESS": 0,
        "INVALID_PARAMS": -1,
        "CONNECTION_POOL_ERROR": -2,
        "SQL_QUERY_ERROR": -3,
        "PERMISSION_DENIED": -5,
        "EXISTS_ALREADY": -6,
        "INVALID_TOKEN": -7,
        "ACCESS_DENIED": -8,
        "INVALID_USER": -9,
        "INTERNAL_SERVER_ERROR": -10,
        "INVALID_SERVER_KEY": -11,
        "INVALID_SERVER_IP": -12,
        "INVALID_OLD_PASSWORD": -13
    },

    responseMessages: {
        "INVALID_PARAMS" : "Invalid parameters",
        "CONNECTION_POOL_ERROR": "Connection pool error",
        "SQL_QUERY_ERROR": "Sql query error",
        "INVALID_USER": "Invalid user",
        "INVALID_USER_NAME": "Invalid username",
        "INVALID_USER_PASSWORD": "Invalid user password",
        "ACCESS_DENIED": "Access denied",
        "INTERNAL_SERVER_ERROR": "Internal Server Error",
        "INVALID_SERVER_KEY": "Invalid Server Key",
        "SUCCESS": "Operation successful",
        "DONE": "Operation successful",
        "INVALID_OLD_PASSWORD": "Invalid old password"
    }

}
module.exports = enums;