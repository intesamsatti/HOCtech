'use strict';
var http = require('http');

class requestHelper {
    static request(url, path, type, body, headers){
        return new Promise(async (resolve, reject)=>{
            var options = {
                host: url,
                port: '9000',
                path: path,
                method: type,
                headers: {
                    token: headers.token,
                    "Content-Type": "application/json"
                }  
            };

            // Make a request to the server
            http.request(options, (res) => {
                let data;
                res.on("data", d => {
                    data += d;
                })
                res.on("end", () => {
                    console.log("ended");
                    resolve(data.toString())
                })
            })
            .on("error", (err) => {
                console.error(err);
                reject();
            })
            .end();  
        });
    }  
}

module.exports = requestHelper;