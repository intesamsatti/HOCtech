const moment = require('moment-timezone');

var timezone = {
    convertAliasToTimezoneDate: (timeStamp, alias) => {
        return new Promise((resolve, reject) => {
            let offset;
            let zoneAlias;
            let date;
            if (alias != null || alias != undefined) {
                let zone = moment(timeStamp*1000).tz(alias);
                if (zone.isValid() && !!moment.tz.zone(alias)) {
                    offset = zone.format('Z');
                    date = zone.format('LLL');
                    zoneAlias = zone.format('z') + " " + alias;
                }else{
                    alias = 'Etc/GMT'
                    zone = moment(timeStamp*1000).tz(alias);
                    offset = '+00:00';
                    date = zone.format('LLL');
                    zoneAlias = zone.format('z') + " " + 'Etc/GMT';
                }   
                let retValue = zone.format('dddd')+ ", " + zone.format('DD') + " " + zone.format('MMMM') +" " + zone.format('YYYY') + " at " + zone.format('LT') + " " + alias + " GMT" + offset
                resolve(retValue);
            } else {
                alias = 'Etc/GMT';
                let zone = moment(timeStamp*1000).tz(alias);
                if (zone.isValid()) {
                    offset = zone.format('Z');
                    date = zone.format('LLL');
                    zoneAlias = zone.format('z') + " " + alias;
                }
                let retValue = zone.format('dddd')+ ", " + zone.format('DD') + " " + zone.format('MMMM') +" " + zone.format('YYYY') + " at " + zone.format('LT') + " " + alias + " GMT" + offset
                resolve(retValue);

            }
        });
    },

    getLocalVisitTimes: (startDateTime, endDateTime, zone) => {
        return new Promise((resolve, reject)=>{
            Promise.all([timezone.convertAliasToTimezoneDate(startDateTime, zone), timezone.convertAliasToTimezoneDate(endDateTime, zone)])
            .then((convertedTimes)=>{
                resolve(convertedTimes);
            })
            .catch((err)=>{   
                reject(err);
            });
        });
    }
}

module.exports = timezone;