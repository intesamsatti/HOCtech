let secret = require('../../config/secret');
let jwt = require('jsonwebtoken');
let EXPIRY_TIME = 60; //days

let tokenUtil = {
    jwtKey: secret.getJwtKey(),
    create: (details) => {
        dateObj = new Date(),
        dateObj = dateObj.setDate(dateObj.getDate() + EXPIRY_TIME);
        return jwt.sign(
            {
                userId: details.userId,
                name: details.name,
                email: details.email,
                expiry: dateObj
            },
            tokenUtil.jwtKey);
    }
}

module.exports = tokenUtil;