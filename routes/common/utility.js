
const crypto = require('crypto');
const appConfig = require("./../../config/appConfig");
const algorithm = "aes256";
const secretKey = appConfig.jwtKey;
const iv = crypto.randomBytes(512).toString('hex');
const validator = require('validator');

let utility = {
    sendResponse: (res, responseCode, message, data) => {
        if(!res) return;
        res.json({
            "statusCode" : responseCode,
            "statusMessage" :  message || "",
            "data": data || ""
        });
    },

    getTimeStamp: () => {
        return Math.floor(new Date().getTime()/1000);
    },
    resolveIPAddress: (hostName = null) => {
        return new Promise((resolve, reject) => {
            if(!hostName) return reject(null);
            let dns = require('dns');
            dns.lookup(hostName, (err, address, family) => {
                if(err){
                    return reject(null);
                } 
                resolve(address);
            });
        });
    },

    encrypt: (text) => {
        text = JSON.stringify(text)
        let key = crypto.createHash('sha256').update(secretKey).digest('base64').substr(0, 32);
        let ivstring = iv.toString('hex').slice(0, 16);
        const cipher = crypto.createCipheriv(algorithm, key, ivstring);
        const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
        return {
            iv: iv.toString('hex'),
            content: encrypted.toString('hex')
        };
    },
    
    decrypt: (hash) => {
        let key = crypto.createHash('sha256').update(secretKey).digest('base64').substr(0, 32);
        let ivstring = hash.iv.toString('hex').slice(0, 16);
        const decipher = crypto.createDecipheriv(algorithm, key, ivstring);
        const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
        return decrpyted.toString();
    },

    isAlphaNumeric: (string) => {
        let expression = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;
        return string.match(expression);
    },

    isValidEmail: (email = null) => {
        if(!email) return false;
        if(!validator.isEmail(email)) return false;
        return true;
    },

    isValidsha256: (sha256String) => {
        let expression = /\b[A-Fa-f0-9]{64}\b/g;
        return sha256String.match(expression);
    }
}

module.exports = utility;