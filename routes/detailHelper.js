const utility = require("./common/utility");
const {responseCodes, responseMessages, licenseStatuses} = require("./common/enums");
const dbContext = require("./common/dbContext");
const crypto =  require("crypto");
const enums = require("./common/enums");
const requestIp = require("request-ip");
const { response } = require("express");
//const get_ip = require("ipware")().get_ip;

let detailHelper = {
    getProjects: (req, res) => {
        let getProjects = `SELECT p.* FROM projects p LEFT JOIN phases ph ON p.id = ph.projectId`;
        dbContext.query(getProjects).then((response) => {
                utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)
            
        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    },

    getReports: (req, res) => {
        let getReport = `SELECT s.name as sName, p.name as pName, SUM(a.amount), SUM(r.amount) FROM sectors s
                        LEFT JOIN projects p ON s.id = p.sectorId
                        LEFT JOIN allocations a ON a.projectId = p.id
                        LEFT JOIN releases r ON r.projectId = p.id
                        GROUP BY p.name`

        dbContext.query(getReport).then((response) => {
            utility.sendResponse(res, responseCodes.SUCCESS, responseMessages.DONE, response)

        }).catch((err) => {
            utility.sendResponse(res, responseCodes.SQL_QUERY_ERROR, responseMessages.SQL_QUERY_ERROR)
        })
    }
}

module.exports = detailHelper;