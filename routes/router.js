const express = require("express");
const router = express.Router();
const detailHelper = require("./detailHelper");
var Sectors = require('./Sectors');
var Projects = require('./Projects');
var Phases = require('./Phases');
var userLoginCheck = require('../middleware/userLoginCheck');


router.post('/api/v1/userlogin', userLoginCheck);
router.post('/api/v1/sectors', Sectors.addSector);
router.get('/api/v1/sectors', Sectors.getSectors);
router.get('/api/v1/projects', Projects.getProjects);
router.post('/api/v1/projects', Projects.addProjects);
router.post('/api/v1/project/edit', Projects.editProject);
router.post('/api/v1/project/status/edit', Projects.editProjectStatus);
router.post('/api/v1/phases', Phases.addPhase);
router.get('/api/v1/phases', Phases.getPhases);
router.get('/api/v1/reports', detailHelper.getReports);


// router.get("/api/v1/license/details", detailHelper.getDetails);

// router.get("/api/v1/license/listing/details", detailHelper.getListing);

// router.post("/api/v1/license/login", licenseHelper.login);

// router.post("/api/v1/license/status/update", detailHelper.changeStatus);

// router.post("/api/v1/license/details/edit", detailHelper.editLicenseServerList);

// router.post("/api/v1/license/regenrate/key", detailHelper.regenrateLicenseKey);

// router.post("/api/v1/license/renew", detailHelper.renewLicenseExpiry);

// router.put("/api/v1/license/change/password", detailHelper.changePassword);


module.exports = router;
