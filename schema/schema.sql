use hoct;
CREATE TABLE IF NOT EXISTS `sectors` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(60) NOT NULL,    
    `createdAt` VARCHAR(45) NOT NULL,
    `updatedAt` VARCHAR(45) NULL,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE IF NOT EXISTS `projects` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `sectorId` BIGINT NOT NULL,
    `name` VARCHAR(60) NOT NULL,
    `approvingAuthority` VARCHAR(60) NOT NULL,
    `adminApprovalDate` VARCHAR(60) NOT NULL,
    `adpCost` BIGINT NULL,
    `adpSno` VARCHAR(45) NULL,
    `status` VARCHAR(45) NULL,    
    `createdAt` VARCHAR(45) NOT NULL,
    `updatedAt` VARCHAR(45) NULL,
    PRIMARY KEY (`id`),
    KEY `licenseServerIndex1` (`name`)
 
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE IF NOT EXISTS `phases` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(60) NOT NULL,
    `projectId` BIGINT NOT NULL,
    `cost` BIGINT NOT NULL,
    `physicalProgress` BIGINT NOT NULL,
    `quantity` VARCHAR(60),
    `createdAt` VARCHAR(45) NOT NULL,
    `updatedAt` VARCHAR(45) NULL,
    PRIMARY KEY (`id`),
    KEY `licenseServerIndex1` (`name`),
    KEY `licenseServerIndex6` (`createdAt`),
    KEY `licenseServerIndex7` (`updatedAt`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE IF NOT EXISTS `allocations` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,    
    `projectId` BIGINT NOT NULL,
    `amount` BIGINT NOT NULL,    
    `createdAt` VARCHAR(45) NOT NULL,
    `updatedAt` VARCHAR(45) NULL,
    PRIMARY KEY (`id`),
    KEY `licenseServerIndex6` (`createdAt`),
    KEY `licenseServerIndex7` (`updatedAt`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE IF NOT EXISTS `releases` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `amount` VARCHAR(60) NOT NULL,
    `projectId` BIGINT NOT NULL,     
    `createdAt` VARCHAR(45) NOT NULL,
    `updatedAt` VARCHAR(45) NULL,
    PRIMARY KEY (`id`),    
    KEY `licenseServerIndex6` (`createdAt`),
    KEY `licenseServerIndex7` (`updatedAt`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE IF NOT EXISTS `expenditure` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(60) NOT NULL,
    `projectId` BIGINT NOT NULL,
    `amount` BIGINT NOT NULL,    
    `createdAt` VARCHAR(45) NOT NULL,
    `updatedAt` VARCHAR(45) NULL,
    PRIMARY KEY (`id`),
    KEY `licenseServerIndex1` (`name`),
    KEY `licenseServerIndex6` (`createdAt`),
    KEY `licenseServerIndex7` (`updatedAt`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE IF NOT EXISTS `user` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `email` VARCHAR(80) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `createdAt` VARCHAR(45) NOT NULL,
    `updatedAt` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `userIndex1` (`name`),
    KEY `userIndex2` (`email`),
    KEY `userIndex3` (`password`),
    KEY `userIndex4` (`createdAt`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

insert into user (name,email,password,createdAt,updatedAt)values("test","test@test.com","test",curdate(),curdate())